/*
Copyright 2016 Paul Williamson <squarefrog@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <util/delay.h>
#include "bootloader.h"
#include "keymap_common.h"


const uint8_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    /*
     * Layer 0: Default Qwerty Layer
     *
     * ,--------------------------------------------------.           ,--------------------------------------------------.
     * |   `    |   1  |   2  |   3  |   4  |   5  |      |           |      |   6  |   7  |   8  |   9  |   0  |   +    |
     * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
     * |  Tab   |   Q  |   W  |   E  |   R  |   T  | +-L3 |           | +-L1 |   Y  |   U  |   I  |   O  |   P  |   -    |
     * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
     * | CpsLk  |   A  |   S  |   D  |   F  |   G  |------|           |------|   H  |   J  |   K  |   L  |   '  |   \    |
     * |--------+------+------+------+------+------| +-L4 |           | +-L2 |------+------+------+------+------+--------|
     * | LShift |   Z  |   X  |   C  |   V  |   B  |      |           |      |   N  |   M  |   ,  |   .  |   /  | RShift |
     * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
     *   | LCtrl| LGui | LAlt |ESC/CT|;/SHFT|                                       |-/SHFT| DEL  | INS  |      | RCtrl |
     *   `----------------------------------'                                       `----------------------------------'
     *                                        ,-------------.       ,-------------.
     *                                        |   Y  |   P  |       |  UP  |PRTSCN|
     *                                 ,------|------|------|       |------+------+------.
     *                                 |   .  | DEL  | ENT  |       | DOWN | LEFT | RGHT |
     *                                 |------|------|------|       |------|------|------|
     *                                 | SP/L3|TB/CTR| BTN3 |       |CTRL+B| BSPC |EN/L4 |
     *                                 `--------------------'       `--------------------'
     */

    KEYMAP(
        // left hand
        GRV, 1,   2,   3,   4,   5,   NO,
        TAB, Q,   W,   E,   R,   T,   FN3,
        CAPS,A,   S,   D,   F,   G,
        LSFT,Z,   X,   C,   V,   B,   FN4,
        LCTL,LGUI,LALT,FN25,FN23,
                                      Y,   P,
                                 DOT, DEL, ENT,
                                 FN20,FN22,BTN3,
        // right hand
             NO,  6,   7,   8,   9,   0,   EQL,
             FN1, Y,   U,   I,   O,   P,   MINS,
                  H,   J,   K,   L,   QUOT,BSLS,
             FN2, N,   M,   COMM,DOT, SLSH,RSFT,
                       FN24,DEL, INS, NO,  RCTL,
        UP,  PSCR,
        DOWN,LEFT,RGHT,
        FN28,BSPC,FN21
    ),

    /*
     * Layer 1: Programmers Workman Layout
     *
     * ,--------------------------------------------------.           ,--------------------------------------------------.
     * |  TRNS  | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS |           | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS   |
     * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
     * |  TRNS  |   Q  |   D  |   R  |   W  |   B  | TRNS |           | TRNS |   J  |   F  |   U  |   P  |   '  | TRNS   |
     * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
     * |  TRNS  |   A  |   S  |   H  |   T  |   G  |------|           |------|   Y  |   N  |   E  |   O  |   I  | TRNS   |
     * |--------+------+------+------+------+------| TRNS |           | TRNS |------+------+------+------+------+--------|
     * |  TRNS  |   Z  |   X  |   M  |   C  |   V  |      |           |      |   K  |   L  |   ,  |   .  |   /  | TRNS   |
     * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
     *   | TRNS | TRNS | TRNS | TRNS | TRNS |                                       | TRNS | TRNS | TRNS | TRNS | TRNS  |
     *   `----------------------------------'                                       `----------------------------------'
     *                                        ,-------------.       ,-------------.
     *                                        | TRNS | TRNS |       | TRNS | TRNS |
     *                                 ,------|------|------|       |------+------+------.
     *                                 | TRNS | TRNS | TRNS |       | TRNS | TRNS | TRNS |
     *                                 |------|------|------|       |------|------|------|
     *                                 | TRNS | TRNS | TRNS |       | TRNS | TRNS | TRNS |
     *                                 `--------------------'       `--------------------'
     */

    KEYMAP(
        // left hand
        TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,Q,   D,   R,   W,   B,   TRNS,
        TRNS,A,   S,   H,   T,   G,
        TRNS,Z,   X,   M,   C,   V,   TRNS,
        TRNS,TRNS,TRNS,TRNS,TRNS,
                                      TRNS,TRNS,
                                 TRNS,TRNS,TRNS,
                                 TRNS,TRNS,TRNS,
        // right hand
             TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
             TRNS,J,   F,   U,   P,   QUOT,TRNS,
                  Y,   N,   E,   O,   I,   TRNS,
             TRNS,K,   L,   COMM,DOT, SLSH,TRNS,
                       TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,
        TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS
    ),

    /*
     * Layer 2: Function Keys
     *
     * ,--------------------------------------------------.           ,--------------------------------------------------.
     * |  TRNS  |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |           |  F7  |  F8  |  F9  | F10  | F11  | F12  | TRNS   |
     * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
     * |  TRNS  | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS |           | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS   |
     * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
     * |  TRNS  | TRNS | TRNS | TRNS | TRNS | TRNS |------|           |------| TRNS | TRNS | TRNS | TRNS | TRNS | TRNS   |
     * |--------+------+------+------+------+------| TRNS |           | TRNS |------+------+------+------+------+--------|
     * |  TRNS  | TRNS | TRNS | TRNS | TRNS | TRNS |      |           |      | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS   |
     * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
     *   | TRNS | TRNS | TRNS | TRNS | TRNS |                                       | TRNS | TRNS | TRNS | TRNS | TRNS  |
     *   `----------------------------------'                                       `----------------------------------'
     *                                        ,-------------.       ,-------------.
     *                                        | TRNS | TRNS |       | TRNS | TRNS |
     *                                 ,------|------|------|       |------+------+------.
     *                                 | TRNS | TRNS | TRNS |       | TRNS | TRNS | TRNS |
     *                                 |------|------|------|       |------|------|------|
     *                                 | TRNS | TRNS | TRNS |       | TRNS | TRNS | TRNS |
     *                                 `--------------------'       `--------------------'
     */

    KEYMAP(
        // left hand
        TRNS,F1,  F2,  F3,  F4,  F5,  F6,
        TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS,TRNS,TRNS,
                                      TRNS,TRNS,
                                 TRNS,TRNS,TRNS,
                                 TRNS,TRNS,TRNS,
        // right hand
             F7,  F8,  F9,  F10, F11, F12, TRNS,
             TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
                  TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
             TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
                       TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,
        TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS
    ),

    /*
     * Layer 3: Symbol Keys
     *
     * ,--------------------------------------------------.           ,--------------------------------------------------.
     * |  TRNS  | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS |           | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS   |
     * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
     * |        |   "  |   '  |   /  |   *  |   \  | TRNS |           | TRNS |   %  |   +  |   -  |   ^  |      |        |
     * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
     * |        |   [  |   ]  |   {  |   }  |   $  |------|           |------|   <  |   =  |   >  |   (  |   )  |        |
     * |--------+------+------+------+------+------| TRNS |           | TRNS |------+------+------+------+------+--------|
     * |        |   ^  |   ~  |   `  |   :  |   .  |      |           |      |   !  |   |  |   &  |   @  |   #  |        |
     * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
     *   |      |      |      |      | TRNS |                                       | TRNS |      |      |      |       |
     *   `----------------------------------'                                       `----------------------------------'
     *                                        ,-------------.       ,-------------.
     *                                        | TRNS | TRNS |       | TRNS | TRNS |
     *                                 ,------|------|------|       |------+------+------.
     *                                 |   ,  | TRNS | TRNS |       | TRNS | TRNS | TRNS |
     *                                 |------|------|------|       |------|------|------|
     *                                 | TRNS | TRNS | TRNS |       | TRNS | TRNS | TRNS |
     *                                 `--------------------'       `--------------------'
     */

    KEYMAP(
        // left hand
        TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
        NO,  FN27,QUOT,SLSH,FN7, BSLS,TRNS,
        NO,  LBRC,RBRC,FN10,FN11,FN6,
        NO,  FN8, GRV, FN5, FN30,DOT, TRNS,
        NO,  NO,  NO,  NO,TRNS,
                                      TRNS,TRNS,
                                 COMM,TRNS,TRNS,
                                 TRNS,TRNS,TRNS,
        // right hand
             TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
             TRNS,FN26,PPLS,MINS,FN8, NO,  NO,
                  FN18,EQL, FN19,FN16,FN17,NO,
             TRNS,FN9, FN13,FN12,FN14,FN15,NO,
                       TRNS,NO,  NO,  NO,  NO,
        TRNS,TRNS,
        TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS
    ),

    /*
     * Layer 4: Arrows, Navigation and Media Keys
     *
     * ,--------------------------------------------------.           ,--------------------------------------------------.
     * |        |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |           |  F7  |  F8  |  F9  | F10  | F11  | F12  |        |
     * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
     * |        | HOME | END  | PGUP | PGDN |      | TRNS |           | TRNS |   -  |   7  |   8  |   9  |   *  |        |
     * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
     * |        | LEFT | RGHT |  UP  | DOWN |      |------|           |------|   0  |   4  |   5  |   6  |   +  |        |
     * |--------+------+------+------+------+------| TRNS |           | TRNS |------+------+------+------+------+--------|
     * |  TRNS  | WHLL | WHLR | WHLU | WHLD |      |      |           |      |   .  |   1  |   2  |   3  |   /  |        |
     * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
     *   | TRNS | TRNS | TRNS | TRNS | TRNS |                                       |   0  |  00  |   =  |      |       |
     *   `----------------------------------'                                       `----------------------------------'
     *                                        ,-------------.       ,-------------.
     *                                        | MUTE | MSTP |       | TRNS | TRNS |
     *                                 ,------|------|------|       |------+------+------.
     *                                 |      | PREV | NEXT |       | TRNS | TRNS | TRNS |
     *                                 |------|------|------|       |------|------|------|
     *                                 | VOLD | VOLU | PLPS |       | TRNS | TRNS | TRNS |
     *                                 `--------------------'       `--------------------'
     */

    KEYMAP(
        // left hand
        NO,  F1,  F2,  F3,  F4,  F5,  F6,
        NO,  HOME,END, PGUP,PGDN,NO,  TRNS,
        NO,  LEFT,RGHT,UP,  DOWN,NO,
        TRNS,WH_L,WH_R,WH_U,WH_D,NO,  TRNS,
        TRNS,TRNS,TRNS,TRNS,TRNS,
                                      MUTE,MSTP,
                                 NO,  MPRV,MNXT,
                                 VOLD,VOLU,MPLY,
        // right hand
             F7,  F8,  F9, F10, F11, F12,  NO,
             TRNS,PMNS,7,   8,   9,   PAST,NO,
                  0,   4,   5,   6,   PPLS,NO,
             TRNS,PDOT,1,   2,   3,   PSLS,NO,
                       0,   NO,  PEQL,NO,  NO,
        TRNS,TRNS,
        TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS
    ),

    /*
     * Layer 2: Function Keys
     *
     * ,--------------------------------------------------.           ,--------------------------------------------------.
     * |  TRNS  | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS |           | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS   |
     * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
     * |  TRNS  | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS |           | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS   |
     * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
     * |  TRNS  | TRNS | TRNS | TRNS | TRNS | TRNS |------|           |------| TRNS | TRNS | TRNS | TRNS | TRNS | TRNS   |
     * |--------+------+------+------+------+------| TRNS |           | TRNS |------+------+------+------+------+--------|
     * |  TRNS  | TRNS | TRNS | TRNS | TRNS | TRNS |      |           |      | TRNS | TRNS | TRNS | TRNS | TRNS | TRNS   |
     * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
     *   | TRNS | TRNS | TRNS | TRNS | TRNS |                                       | TRNS | TRNS | TRNS | TRNS | TRNS  |
     *   `----------------------------------'                                       `----------------------------------'
     *                                        ,-------------.       ,-------------.
     *                                        | TRNS | TRNS |       | TRNS | TRNS |
     *                                 ,------|------|------|       |------+------+------.
     *                                 | TRNS | TRNS | TRNS |       | TRNS | TRNS | TRNS |
     *                                 |------|------|------|       |------|------|------|
     *                                 | TRNS | TRNS | TRNS |       | TRNS | TRNS | TRNS |
     *                                 `--------------------'       `--------------------'
     */

    /*KEYMAP(
        // left hand
        TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS,TRNS,TRNS,
                                      TRNS,TRNS,
                                 TRNS,TRNS,TRNS,
                                 TRNS,TRNS,TRNS,
        // right hand
             TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
             TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
                  TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
             TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,TRNS,
                       TRNS,TRNS,TRNS,TRNS,TRNS,
        TRNS,TRNS,
        TRNS,TRNS,TRNS,
        TRNS,TRNS,TRNS
    ),*/
};

/* id for user defined functions */
enum function_id {
    TEENSY_KEY,
};

/*
 * Fn action definition
 */
const action_t PROGMEM fn_actions[] = {
    // Layer shifting
    [1] = ACTION_LAYER_TAP_TOGGLE(1),
    [2] = ACTION_LAYER_TAP_TOGGLE(2),
    [3] = ACTION_LAYER_TAP_TOGGLE(3),
    [4] = ACTION_LAYER_TAP_TOGGLE(4),


    // Mod keys
    [5] = ACTION_MODS_KEY(MOD_LSFT, KC_GRV),
    [6] = ACTION_MODS_KEY(MOD_LSFT, KC_4),
    [7] = ACTION_MODS_KEY(MOD_LSFT, KC_8),
    [8] = ACTION_MODS_KEY(MOD_LSFT, KC_6),
    [9] = ACTION_MODS_KEY(MOD_LSFT, KC_1),
    [10] = ACTION_MODS_KEY(MOD_LSFT, KC_LBRC),
    [11] = ACTION_MODS_KEY(MOD_LSFT, KC_RBRC),
    [12] = ACTION_MODS_KEY(MOD_LSFT, KC_7),
    [13] = ACTION_MODS_KEY(MOD_LSFT, KC_BSLS),
    [14] = ACTION_MODS_KEY(MOD_LSFT, KC_2),
    [15] = ACTION_MODS_KEY(MOD_LSFT, KC_3),
    [16] = ACTION_MODS_KEY(MOD_LSFT, KC_9),
    [17] = ACTION_MODS_KEY(MOD_LSFT, KC_0),
    [18] = ACTION_MODS_KEY(MOD_LSFT, KC_COMM),
    [19] = ACTION_MODS_KEY(MOD_LSFT, KC_DOT),
    [28] = ACTION_MODS_KEY(MOD_LCTL, KC_B),
    [27] = ACTION_MODS_KEY(MOD_LSFT, KC_QUOT),
    [26] = ACTION_MODS_KEY(MOD_LSFT, KC_5),
    [30] = ACTION_MODS_KEY(MOD_LSFT, KC_SCLN),


    // Dual-use Keys
    [20] = ACTION_LAYER_TAP_KEY(3, KC_SPC),
    [21] = ACTION_LAYER_TAP_KEY(4, KC_ENT),
    [22] = ACTION_MODS_TAP_KEY(MOD_LCTL, KC_TAB),
    [25] = ACTION_MODS_TAP_KEY(MOD_LCTL, KC_ESC),
    [23] = ACTION_MODS_TAP_KEY(MOD_LSFT, KC_SCLN),
    [24] = ACTION_MODS_TAP_KEY(MOD_RSFT, KC_MINS),

    // Teensy
    [31] = ACTION_FUNCTION(TEENSY_KEY),
};

void action_function(keyrecord_t *event, uint8_t id, uint8_t opt)
{
    if (id == TEENSY_KEY) {
        clear_keyboard();
        print("\n\nJump to bootloader... ");
        _delay_ms(250);
        bootloader_jump(); // should not return
        print("not supported.\n");
    }
}

